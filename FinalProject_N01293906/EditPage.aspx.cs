﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProject_N01293906
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int page_id
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView pagerow = getPageInfo();
            if (pagerow == null)
            {
                page_name.InnerHtml = "No Student Found.";
                return;
            }
            page_title.Text = pagerow["pagetitle"].ToString();
            page_author.Text = pagerow["pageauthor"].ToString();
            //put this into the title so we know who we're editing
            if (!Page.IsPostBack)
            {
                page_name.InnerHtml += page_title.Text + " " + page_author.Text;

            }
            page_content.Text = pagerow["pagecontent"].ToString();

        }
        protected void Edit_Page(object sender, EventArgs e)
        {
            string p_title = page_title.Text;
            string p_author = page_author.Text;
            string p_content = page_content.Text;

            string editquery = "Update blogpages set pagetitle='" + p_title + "'," +
                "pageauthor='" + p_author + "','" +
                "pagecontent='" + p_content + "' where pageid = "+page_id;
            edit_page.UpdateCommand = editquery;
            edit_page.Update();
        }
        protected DataRowView getPageInfo()
        {
            string query = "select * from blogpages where pageid=" + page_id.ToString();
            page_select.SelectCommand = query;
            //debug.InnerHtml = query;
            //Another small manual manipulation to present the pages name outside of
            //the datagrid.
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            //If there is no page in the pageview, an invalid id is passed
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; //gets the first result which is always the student
            return pagerowview;

        }
    }
}