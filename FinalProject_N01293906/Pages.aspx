﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="FinalProject_N01293906.Pages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <h2><%: Title %></h2>
    <!-- interface for adding new students -->
    <div id="inputrow">
        <a href="NewPage.aspx">New Page</a>
    </div>
    <!-- Interface for searching BLOGPAGES records-->
    <asp:TextBox runat="server" ID="page_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Pages"/>
    <%-- 
        Connection string for built in SQL system
    --%>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="fullscreen" runat="server">
<asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>
</asp:Content>
