﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;

namespace FinalProject_N01293906.UserControls
{
    public partial class NavBar : System.Web.UI.UserControl
    { //base query
        private string basequery = "SELECT pageid, pagetitle from blogpages";
        

        private int selected_id; //Actual field
        public int _selected_id // Accessor
        {
            //standard property accessors
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //We don't need to add onto the base query.
            //However, if we were imagining a more advanced app,
            //We could imagine showing classes that the particular
            //student is not yet enrolled in!
            //The narrative builds the code!

            pages_list_pick.SelectCommand = basequery;
            //Did you think that datagrids were the only
            //Thing you could bind to an element?
            //However, we need to render it manually (again).

            //Instead of converting a datasource we instead
            //Manually add items onto the list.
            Pages_Manual_Bind(pages_list_pick, "page_link");

        }

        //take an id (of a dropdownlist) and a datasource.
        //iterate through the datasource and add list items
        //to the dropdownlist.
        void Pages_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            //The rendering loop here is to print out list items
            //For our asp drop down list.
            String link = "Page.aspx?pageid=";

                
                DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                page_link.Controls.Add(li);
                string p_id = row["pageid"].ToString();
                link += p_id;
                Console.WriteLine(p_id);
                string pagetitle = row["pagetitle"].ToString();
                HtmlGenericControl anchor = new HtmlGenericControl("a");
                anchor.Attributes.Add("href", link);
                anchor.InnerText = pagetitle;

                li.Controls.Add(anchor);
                link = "Page.aspx?pageid=";
            }
        }

    }
}