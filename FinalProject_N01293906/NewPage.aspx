﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="FinalProject_N01293906.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <asp:SqlDataSource runat="server" id="create_page"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <h3>New Page</h3>

    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="page_title" runat="server"> </asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_title" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Title" ControlToValidate="page_title"></asp:RequiredFieldValidator> 
    </div>

    <div class="inputrow">
        <label>Author Name:</label>
        <asp:textbox id="page_author" runat="server"></asp:textbox>
        <asp:RequiredFieldValidator ID="validator_page_author" ForeColor="Red" runat="server" ErrorMessage="Must Enter a Author" ControlToValidate="page_author"></asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <label>Content:</label>
        <asp:textbox id="page_content" TextMode="multiline" Columns="50" Rows="5" runat="server">
        </asp:textbox>
    </div>

    <asp:Button Text="Create Page" runat="server" OnClick="CreatePage"/>

</asp:Content>
