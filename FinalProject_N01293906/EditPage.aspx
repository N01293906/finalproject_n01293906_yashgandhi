﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FinalProject_N01293906.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <%-- One SQL command for viewing the existing info --%>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <%-- One SQL command for editing the student --%>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:blog_sql_con %>">
    </asp:SqlDataSource>

    <h3 runat="server" id="page_name">Update </h3>

    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="page_title" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Author:</label>
        <asp:textbox id="page_author" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Page Content:</label>
        <asp:textbox id="page_content" TextMode="multiline" Columns="50" Rows="5" runat="server">
        </asp:textbox>
    </div>
     <div>
    <asp:Button Text="Edit" runat="server" OnClick="Edit_Page"/>
    </div>

</asp:Content>
