﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProject_N01293906
{
    public partial class Page : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        private string page_basequery =
            "SELECT * from BLOGPAGES";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (pageid == "" || pageid == null)
                page_title.InnerHtml = "No class found.";
            else
            {
                //Need to use custom rendering on this one to get the class link
                page_basequery += " WHERE PAGEID = " + pageid;
                page_select.SelectCommand = page_basequery;
                

                //Another small manual manipulation to present the students name outside of
                //the datagrid.
                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
                //Since we're operating on a primary key we can guarantee we only get 1 row
                foreach (DataRowView row in pageview)
                {
                    string pagetitle = row["pagetitle"].ToString();
                    string pagecontent = row["pagecontent"].ToString();
                    string pagedate = row["pagedate"].ToString();
                    page_title.InnerHtml = pagetitle;
                    page_content.InnerHtml = pagecontent;
                    page_date.InnerHtml = pagedate;
                }
               // DataRowView pagerowview = pageview[0]; //gets the first result which is always the student   

            }
        }
        protected void FetchID(object sender, EventArgs e)
        {
            
            DelPage(pageid);

        }
        protected void DelPage(string pageid)
        {
            string basequery = "DELETE FROM BLOGPAGES WHERE PAGEID=" + pageid;
            del_page.DeleteCommand = basequery;
            del_page.Delete();
           // Response.Redirect()

        }
    }
}