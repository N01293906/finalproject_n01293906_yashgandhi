﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProject_N01293906
{
    public partial class Pages : System.Web.UI.Page
    {
        private string sqlline = "SELECT pageid, pagetitle as Title, pageauthor as AUTHOR, convert(varchar,pagedate,106) as 'PUBLISH DATE' FROM blogpages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = sqlline;

            pages_list.DataSource = Pages_Manual_Bind(pages_select);

            //To append an extra button we have to
            //disable the auto generate columns
            //bind the SQL result set to our datagrid manually... again

            /*BoundColumn s_id = new BoundColumn();
            s_id.DataField = "pageid";
            s_id.HeaderText = "ID";
            s_id.Visible = false;
            pages_list.Columns.Add(s_id);


            BoundColumn name = new BoundColumn();
            name.DataField ="pagetitle";
            name.HeaderText = "Title";
            pages_list.Columns.Add(name);

            BoundColumn number = new BoundColumn();
            number.DataField = "pageauthor";
            number.HeaderText = "Author";
            pages_list.Columns.Add(number);

            BoundColumn enrolled = new BoundColumn();
            enrolled.DataField = "pagedate";
            enrolled.HeaderText = "Publish Date";
            pages_list.Columns.Add(enrolled);*/

            pages_list.DataBind();
        }
        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            //Don't like the way DataBind() works? Do it yourself!
            //Thisfunction will returna dataview that is substitute for
            //the automatic "dataview" provided by sqldatasource
            DataTable mytbl;

            //Eventually we will bind the data to this variable
            DataView myview;

            //First we need to make a table that pulls from the datasource
            //Sometimes working with a microsoft development environment
            //yields playing silly games with their datatypes and classes
            //to do straightforward transformations
            //ie. instead of DataSource > DataTable
            // microsoft makes us do DataSource > DataView > DataTable
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["TITLE"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["TITLE"]
                    + "</a>";

            }
            //Going to hide the pageid instead of removing it
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;

        }
        protected void Search_Pages(object sender, EventArgs e)
        {
            /*
              This function takes the inputs from the search
              and changes the sqldatasource select command
              to filter down students.
            */
            string newsql = sqlline + " WHERE (1=1)";
            string key = page_key.Text;

            if (key != "")
            {
                newsql +=
                    " AND PAGETITLE LIKE '%" + key + "%'" + " OR PAGEAUTHOR LIKE '%" + key + "%') ";
            }
            
            pages_select.SelectCommand = newsql;
            //pages_query.InnerHtml = newsql;

            //Intercept the "auto bind" and create our own binding
            pages_list.DataSource = Pages_Manual_Bind(pages_select);

            pages_list.DataBind();

        }

    }
}