﻿--------------------------------------------------------
--  DDL for Table BLOGPAGES
--------------------------------------------------------

  CREATE TABLE "BLOGPAGES" 
   (	"PAGEID" BIGINT IDENTITY(1,1) PRIMARY KEY, 
	"PAGETITLE" VARCHAR(50), 
	"PAGECONTENT" VARCHAR(MAX),
	"PAGEAUTHOR" VARCHAR(50),
	"PAGEDATE" DATETIME
   );
-- INSERTING into STUDENTSXCLASSES
/* SET DEFINE OFF; */
